var https = require("https");
var lastPlayers;

(function check(){
	https.get("https://bitfighter.org/bitfighterStatus.json", function(res) {
		var body = "";
		res.on("data", x => body += x);
		res.on("end", () => {
			try {
				var status = JSON.parse(body);
				var players = status.players;
				if (!lastPlayers) return lastPlayers = players;
				for (let player of players) {
					if (!lastPlayers.includes(player)) {
						// player joined
						send(`**${player}** joined 😄`);
					}
				}
				for (let lastPlayer of lastPlayers) {
					if (!players.includes(lastPlayer)) {
						// player left
						send(`**${lastPlayer}** left 😦`);
					}
				}
				lastPlayers = players;
			} catch (error) {
				console.log(error.message);
			}
			function send(content) {
				content = content.replace(/@/g, "@\u2063");
				var payload = JSON.stringify({content, embeds:[{
					color: 65280,
					/*fields: [
						{
							"name": t(`${status.players.length} players online now:`, 256),
							"value": t(status.players.map(x => status.authenticated[status.players.indexOf(x)] ? `**${x}**` : x).join(', ') || '👻', 1024)
						},
						{
							"name": t(`Servers Online (${status.serverCount}):`, 256),
							"value": t(status.servers.map(server => `\`${server.serverName}\`: **${server.playerCount}** players playing **${server.currentLevelType}** in **${server.currentLevelName}**`).join('\n') || '👻', 1024)
						}
					],*/
					description: (status.players.length ? `**${status.playerCount}/${status.players.length} player${status.players.length == 1 ? " is" : "s are"} now playing:**\n${status.players.map(p => status.authenticated[status.players.indexOf(p)] ? p : `~~${p}~~`).join(", ")}` : `No more players online 😭`) + '\n\n' +
								 status.servers.filter(s => s.playerCount > 0).map(s => `**${s.playerCount}** player${s.playerCount == 1 ? " is" : "s are"} playing **${s.currentLevelName}** in \`${s.serverName}\``).join('\n')
					//footer: {text: t(status.motd, 2048)}
				}]});
				var req = https.request("redacted webhook url", {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						//"Content-Length": payload.length
					}
				}, function(res) {
					var body = "";
					res.on("data", x => body += x);
					res.on("end", () => {
						if (!String(res.statusCode).startsWith('2')) console.error(res.statusCode, body);
					});
				});
				req.on("error", error => console.error(error.message));
				req.write(payload);
				req.end();
			}
		});
	}).on("error", error => console.error(error.message));
	setTimeout(check, 10000);
})();

function t(x,n) {
	if (x.length > n) {
		return x = x.substr(n - 1) + '…';
	}
	else return x
}
